# Dealer Scraper
The goal of this tool is to uncover the top three worst offenders of overly positive reviews of a car dealearship planted by Russians in the United States.

## Criterias
The criteria used in this tool to uncover these reviews are:
- return only reviews that are recommending the car dealer;
- return reviews that has the maximum rating average (which is 5.0 'stars');
- return reviews containing the most employee reviews (ratings of employee reviews are not considered);

## Usage
This tool requires [Ruby](https://rubylang.org/) 2.7.1 to run.

Install the dependencies:
```sh
$ cd drs
$ bundle install
```
Run the script:
```sh
$ ruby scrape_dealer.rb
```

## Tests
To run the test suite:

```sh
$ rspec
```

## Future Improvements
- add parameters to scrape command to allow more flexibility (e.g scrape from page X to page Y);
- create own DSL to scrape pages to be able to replace lib without changing the implementation;
- use page objects to remove the scraper search details (css classes, html elements, etc.) from the entities builders;
