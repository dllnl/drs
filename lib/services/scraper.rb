class Scraper
  def initialize(scraper: Mechanize.new, base_url:, pagination_param:)
    @scraper = scraper
    @base_url = base_url
    @pagination_param = pagination_param
  end

  def get_from_page(number)
    scraper.get(page_url_for(number))
  end

  private

  attr_reader :scraper, :base_url, :pagination_param

  def page_url_for(number)
    [base_url, pagination_param, number].join
  end
end
