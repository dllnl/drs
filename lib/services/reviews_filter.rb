class ReviewsFilter
  class << self
    def by_severity(reviews)
      reviews = only_recommended(reviews)
      reviews = max_rating_average(reviews)
      reviews = max_employee_reviews(reviews)
      max_reviews(reviews)
    end

    def only_recommended(reviews)
      reviews.select(&:recommended_dealer)
    end

    def max_rating_average(reviews)
      reviews.select { |review| review.rating.eql?(max_rating) }
    end

    def max_employee_reviews(reviews)
      reviews.sort_by { |r| r.employee_reviews.count }.reverse
    end

    def max_reviews(reviews)
      reviews.first(max_reviews_allowed)
    end

    private

    def max_rating
      5.0
    end

    def max_reviews_allowed
      3
    end
  end
end
