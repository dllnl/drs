class NoEmployeeReview
  def employee_id
    '<No Employee ID>'
  end

  def name
    '<No Employee Name>'
  end

  def rating
    NoRating.new
  end
end
