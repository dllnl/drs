class NoRating
  def type
    '<No Rating>'
  end

  def value
    0.0
  end
end
