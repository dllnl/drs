class NoReview
  def date
    '<No Date>'
  end

  def rating
    0.0
  end

  def service
    '<No Service>'
  end

  def author
    '<No Author>'
  end

  def title
    '<No Title>'
  end

  def body
    '<No Body>'
  end

  def recommended_dealer
    false
  end

  def ratings
    []
  end

  def employee_reviews
    []
  end
end
