class RatingBuilder
  def initialize(node)
    @elements = node.search('.td')
  end

  def build
    return NoRating.new if elements.empty?

    Rating.new.tap do |rating|
      rating.type = type
      rating.value = value
    end
  end

  private

  attr_reader :elements

  def type
    elements.first.text
  end

  def value
    elements.last.values.first.scan(/\d+/).first.to_f / 10
  end
end
