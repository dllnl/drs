class ReviewBuilder
  def initialize(node)
    @elements = node.elements
    @side = node.search('.review-date')
    @wrapper = node.search('.review-wrapper')
    @ratings_table = node.search('.review-ratings-all .table .tr')
    @employees_table = node.search('.employees-wrapper .review-employee')
    @recommendation = ratings_table.pop
  end

  def build
    return NoReview.new if elements.empty?

    Review.new.tap do |review|
      review.date = date
      review.rating = rating
      review.service = service
      review.author = author
      review.title = title
      review.body = body
      review.recommended_dealer = recommended_dealer
      review.ratings = ratings
      review.employee_reviews = employee_reviews
    end
  end

  private

  attr_reader :elements, :side, :wrapper, :ratings_table, :employees_table, :recommendation

  def date
    side.children[1].text
  end

  def rating
    side.search('.rating-static').last.values.first.scan(/\d+/).first.to_f / 10
  end

  def service
    side.search('.review-date').children[3].text.strip.gsub(/\s+/, ' ')
  end

  def author
    wrapper.search('span').first.text.sub(/-/, ' ').strip
  end

  def title
    wrapper.search('h3').text.gsub!(/^\"|\"?$/, '')
  end

  def body
    wrapper.search('.review-content').text.gsub(/\s+/, ' ')
  end

  def recommended_dealer
    recommendation.search('.td').last.text.upcase.include?('YES')
  end

  def ratings
    ratings_table.map(&method(:build_rating))
  end

  def build_rating(entry)
    RatingBuilder.new(entry).build
  end

  def employee_reviews
    employees_table.map(&method(:build_employee_review))
  end

  def build_employee_review(entry)
    EmployeeReviewBuilder.new(entry).build
  end
end
