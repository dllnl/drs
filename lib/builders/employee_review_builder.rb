class EmployeeReviewBuilder
  def initialize(node)
    @elements = node.search('.table')
  end

  def build
    return NoEmployeeReview.new if elements.empty?

    EmployeeReview.new.tap do |employee_review|
      employee_review.employee_id = employee_id
      employee_review.name = name
      employee_review.rating = rating
    end
  end

  private

  attr_reader :elements

  def employee_id
    elements.search('a')[0]["data-emp-id"]
  end

  def name
    elements.search('a').text.strip
  end

  def rating
    EmployeeRatingBuilder.new(elements).build
  end
end
