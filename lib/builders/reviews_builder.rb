class ReviewsBuilder
  def initialize(page)
    @elements = page.search('.review-entry')
  end

  def build
    elements.flat_map(&method(:build_review))
  end

  private

  attr_reader :elements

  def build_review(node)
    ReviewBuilder.new(node).build
  end
end
