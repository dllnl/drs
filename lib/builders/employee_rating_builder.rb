class EmployeeRatingBuilder < RatingBuilder
  def initialize(node)
    @elements = node.search('.rating-static')
  end

  private

  def type
    'Employee Review'
  end

  def value
    elements.first.values.first.scan(/\d+/).first.to_f / 10
  end
end
