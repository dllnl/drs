require 'spec_helper'
require 'mechanize'
require_relative '../../lib/services/scraper'

describe Scraper do
  subject { described_class.new(scraper: scraper, base_url: base_url, pagination_param: pagination_param) }

  describe '#get_from_page number' do
    context 'when hitting real target' do
      let(:scraper) { Mechanize.new }
      let(:pagination_param) { '/page' }
      let(:page_number) { 4 }

      let(:base_url) do
        'https://www.dealerrater.com/dealer/McKaig-Chevrolet-Buick-A-Dealer-For-The-People-dealer-reviews-23685'
      end

      it 'downloads base url html from page number' do
        VCR.use_cassette('services/scraper/success') do
          result = subject.get_from_page(page_number)

          expect(result.body).to include('McKaig Chevrolet Buick - A Dealer For The People')
        end
      end
    end
  end
end
