require 'spec_helper'

require_relative '../../lib/services/reviews_filter'
require_relative '../../lib/builders/reviews_builder'
require_relative '../../lib/builders/review_builder'
require_relative '../../lib/builders/rating_builder'
require_relative '../../lib/builders/employee_review_builder'
require_relative '../../lib/builders/employee_rating_builder'
require_relative '../../lib/null_objects/no_review'
require_relative '../../lib/null_objects/no_employee_review'
require_relative '../../lib/models/review'
require_relative '../../lib/models/rating'
require_relative '../../lib/models/employee_review'

describe ReviewsFilter, type: :filter do
  let(:page) { load_node_from('fixtures/builders/reviews/collection.html') }
  let(:reviews) { ReviewsBuilder.new(page).build }

  describe '.by_severity reviews' do
    it 'returns only recommended reviews, max rated with most employee reviews' do
      result = described_class.by_severity(reviews)

      first = result.first
      second = result[1]
      last = result.last

      expect(result.count).to eql(3)
      expect(first.rating).to eql(5.0)
      expect(first.employee_reviews.count).to eql(8)
      expect(second.rating).to eql(5.0)
      expect(second.employee_reviews.count).to eql(6)
      expect(last.rating).to eql(5.0)
      expect(last.employee_reviews.count).to eql(3)
    end
  end

  describe '.only_recommended reviews' do
    it 'returns only reviews with recommended dealers' do
      result = described_class.only_recommended(reviews)

      expect(result.count).to eql(10)
      expect(result.map(&:recommended_dealer).all?).to be true
    end
  end

  describe '.max_rating_average reviews' do
    it 'returns reviews with max rating average' do
      result = described_class.max_rating_average(reviews)

      expect(result.count).to eql(8)
      expect(result.first.rating).to eql(5.0)
      expect(result.last.rating).to eql(5.0)
    end
  end

  describe '.max_employee_reviews reviews' do
    it 'orders reviews based on max employee reviews count' do
      result = described_class.max_employee_reviews(reviews)

      first = result.first
      second = result[1]
      last = result.last

      expect(result.count).to eql(10)
      expect(first.employee_reviews.count).to eql(8)
      expect(second.employee_reviews.count).to eql(6)
      expect(last.employee_reviews.count).to eql(1)
    end
  end

  describe '.max_reviews reviews' do
    it 'returns max reviews allowed' do
      result = described_class.max_reviews(reviews)

      expect(result.count).to eql(3)
    end
  end
end

