require 'spec_helper'
require_relative '../../lib/models/review'

describe Review do
  it_behaves_like 'accessors attributes', %i[
    date rating service author title body ratings employee_reviews recommended_dealer
  ]
end
