require 'spec_helper'
require_relative '../../lib/models/employee_review'

describe EmployeeReview do
  it_behaves_like 'accessors attributes', %i[employee_id name rating]
end
