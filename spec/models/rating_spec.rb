require 'spec_helper'
require_relative '../../lib/models/rating'

describe Rating do
  it_behaves_like 'accessors attributes', %i[type value]
end
