require 'spec_helper'
require_relative '../../lib/null_objects/no_review'

describe NoReview do
  describe '#date' do
    it 'returns no date text' do
      expect(subject.date).to eql('<No Date>')
    end
  end

  describe '#rating' do
    it 'returns zero' do
      expect(subject.rating).to eql(0.0)
    end
  end

  describe '#service' do
    it 'returns no service text' do
      expect(subject.service).to eql('<No Service>')
    end
  end

  describe '#author' do
    it 'returns no author text' do
      expect(subject.author).to eql('<No Author>')
    end
  end

  describe '#title' do
    it 'returns no title text' do
      expect(subject.title).to eql('<No Title>')
    end
  end

  describe '#body' do
    it 'returns no body text' do
      expect(subject.body).to eql('<No Body>')
    end
  end

  describe '#recommended_dealer' do
    it 'returns false' do
      expect(subject.recommended_dealer).to be false
    end
  end

  describe '#ratings' do
    it 'returns empty array' do
      expect(subject.ratings).to be_empty
    end
  end

  describe '#employee_reviews' do
    it 'returns empty array' do
      expect(subject.employee_reviews).to be_empty
    end
  end
end
