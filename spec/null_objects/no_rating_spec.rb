require 'spec_helper'
require_relative '../../lib/null_objects/no_rating'

describe NoRating do
  describe '#type' do
    it 'returns no rating text' do
      expect(subject.type).to eql('<No Rating>')
    end
  end

  describe '#value' do
    it 'returns zero' do
      expect(subject.value).to eql(0.0)
    end
  end
end
