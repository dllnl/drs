require 'spec_helper'
require_relative '../../lib/null_objects/no_employee_review'
require_relative '../../lib/null_objects/no_rating'

describe NoEmployeeReview do
  describe '#employee_id' do
    it 'returns no employee id text' do
      expect(subject.employee_id).to eql('<No Employee ID>')
    end
  end

  describe '#name' do
    it 'returns no name text' do
      expect(subject.name).to eql('<No Employee Name>')
    end
  end

  describe '#rating' do
    it 'returns no rating' do
      expect(subject.rating).to be_a(NoRating)
    end
  end
end
