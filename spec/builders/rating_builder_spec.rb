require 'spec_helper'

require_relative '../../lib/builders/rating_builder'
require_relative '../../lib/null_objects/no_rating'
require_relative '../../lib/models/rating'

describe RatingBuilder, type: :builder do
  subject { described_class.new(node) }

  describe '#build' do
    context 'when rating node has a value' do
      let(:node) { load_node_from('fixtures/builders/ratings/success.html') }

      it 'returns rating object with filled attributes' do
        result = subject.build

        expect(result).to be_a(Rating)
        expect(result.type).to eql('Customer Service')
        expect(result.value).to eql(5.0)
      end
    end

    context 'when rating node has no value' do
      let(:node) { load_node_from('fixtures/builders/ratings/success2.html') }

      it 'returns rating object with zero as fallback value' do
        result = subject.build

        expect(result).to be_a(Rating)
        expect(result.type).to eql('Foo Bar')
        expect(result.value).to eql(0.0)
      end
    end

    context 'when rating node has no elements' do
      let(:node) { load_node_from('fixtures/builders/ratings/failure.html') }

      it 'returns no rating object' do
        result = subject.build

        expect(result).to be_a(NoRating)
        expect(result.type).to eql('<No Rating>')
        expect(result.value).to eql(0.0)
      end
    end
  end
end
