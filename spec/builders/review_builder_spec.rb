require 'spec_helper'

require_relative '../../lib/builders/review_builder'
require_relative '../../lib/builders/rating_builder'
require_relative '../../lib/builders/employee_review_builder'
require_relative '../../lib/builders/employee_rating_builder'
require_relative '../../lib/null_objects/no_review'
require_relative '../../lib/null_objects/no_employee_review'
require_relative '../../lib/models/review'
require_relative '../../lib/models/rating'
require_relative '../../lib/models/employee_review'

describe ReviewBuilder, type: :builder do
  subject { described_class.new(node) }

  describe '#build' do
    context 'when rating node has a value' do
      let(:node) { load_node_from('fixtures/builders/reviews/success.html').search('.review-entry').first }

      it 'returns review object with filled attributes' do
        result = subject.build
        ratings = result.ratings
        employee_reviews = result.employee_reviews

        expect(result).to be_a(Review)
        expect(result.date).to eql('February 19, 2021')
        expect(result.rating).to eql(4.8)
        expect(result.service).to eql('SALES VISIT - USED')
        expect(result.author).to eql('Trestoncoleman')
        expect(result.title).to eql('Great customer service, and even greater people. They...')
        expect(result.body).to include('They won’t stop until you drive away in your dream car or truck.')
        expect(result.recommended_dealer).to be true
        expect(ratings).not_to be_empty
        expect(ratings.first).to be_a(Rating)
        expect(ratings.first.type).to eql('Customer Service')
        expect(ratings.first.value).to eql(5.0)
        expect(employee_reviews).not_to be_empty
        expect(employee_reviews.first).to be_a(EmployeeReview)
        expect(employee_reviews.first.employee_id).to eql('670351')
        expect(employee_reviews.first.name).to eql('Kristina Manning')
        expect(employee_reviews.first.rating).to be_a(Rating)
      end
    end

    context 'when review node has no elements' do
      let(:node) { load_node_from('fixtures/builders/reviews/failure.html').search('.review-entry').first }

      it 'returns no rating object' do
        result = subject.build

        expect(result).to be_a(NoReview)
        expect(result.date).to eql('<No Date>')
        expect(result.rating).to eql(0.0)
        expect(result.service).to eql('<No Service>')
        expect(result.author).to eql('<No Author>')
        expect(result.title).to eql('<No Title>')
        expect(result.body).to eql('<No Body>')
        expect(result.recommended_dealer).to be false
        expect(result.ratings).to be_empty
        expect(result.employee_reviews).to be_empty
      end
    end
  end
end
