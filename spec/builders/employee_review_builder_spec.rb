require 'spec_helper'

require_relative '../../lib/builders/employee_review_builder'
require_relative '../../lib/builders/rating_builder'
require_relative '../../lib/builders/employee_rating_builder'
require_relative '../../lib/null_objects/no_rating'
require_relative '../../lib/null_objects/no_employee_review'
require_relative '../../lib/models/employee_review'
require_relative '../../lib/models/rating'

describe EmployeeReviewBuilder, type: :builder do
  subject { described_class.new(node) }

  describe '#build' do
    context 'when rating node has a value' do
      let(:node) { load_node_from('fixtures/builders/employee_reviews/success.html') }

      it 'returns employee review object with filled attributes' do
        result = subject.build
        rating = result.rating

        expect(result).to be_a(EmployeeReview)
        expect(result.employee_id).to eql('123442')
        expect(result.name).to eql('John Doe')
        expect(rating).to be_a(Rating)
        expect(rating.type).to eql('Employee Review')
        expect(rating.value).to eql(5.0)
      end
    end

    context 'when employee review node has no elements' do
      let(:node) { load_node_from('fixtures/builders/employee_reviews/failure.html') }

      it 'returns no rating object' do
        result = subject.build

        expect(result).to be_a(NoEmployeeReview)
        expect(result.employee_id).to eql('<No Employee ID>')
        expect(result.name).to eql('<No Employee Name>')
        expect(result.rating).to be_a(NoRating)
      end
    end
  end
end
