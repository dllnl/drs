require 'spec_helper'

require_relative '../../lib/builders/reviews_builder'
require_relative '../../lib/builders/review_builder'
require_relative '../../lib/builders/rating_builder'
require_relative '../../lib/builders/employee_review_builder'
require_relative '../../lib/builders/employee_rating_builder'
require_relative '../../lib/null_objects/no_review'
require_relative '../../lib/null_objects/no_employee_review'
require_relative '../../lib/models/review'
require_relative '../../lib/models/rating'
require_relative '../../lib/models/employee_review'

describe ReviewsBuilder, type: :builder do
  subject { described_class.new(page) }

  describe '#build' do
    let(:page) { load_node_from('fixtures/builders/reviews/collection.html') }

    it 'returns reviews collection' do
      result = subject.build
      review = result.last

      expect(result.count).to eql(10)
      expect(review).to be_a(Review)
      expect(review.date).to eql('February 01, 2021')
      expect(review.author).to eql('Wisdom n jacklyn')
      expect(review.rating).to eql(4.8)
      expect(review.ratings.count).to eql(5)
    end
  end
end
