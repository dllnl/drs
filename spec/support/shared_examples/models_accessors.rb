shared_examples_for 'accessors attributes' do |attrs|
  attrs.each do |attr|
    writer_attr = "#{attr}="

    it { is_expected.to respond_to(attr) }
    it { is_expected.to respond_to(writer_attr) }
  end
end
