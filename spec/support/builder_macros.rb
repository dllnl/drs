require 'mechanize'

module BuilderMacros
  def load_node_from(path)
    Mechanize.new.get("file://#{Dir.pwd}/spec/#{path}")
  end
end
