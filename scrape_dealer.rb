require 'mechanize'

%w[
  lib/null_objects/no_employee_review.rb
  lib/null_objects/no_rating.rb
  lib/null_objects/no_review.rb
  lib/models/review.rb
  lib/models/rating.rb
  lib/models/employee_review.rb
  lib/builders/rating_builder.rb
  lib/builders/employee_review_builder.rb
  lib/builders/review_builder.rb
  lib/builders/reviews_builder.rb
  lib/builders/employee_rating_builder.rb
  lib/services/scraper.rb
  lib/services/reviews_filter.rb
].each { |f| require_relative f }

class Application
  class << self
    def run
      reviews = scrape_reviews
      reviews = filter_reviews(reviews)
      print_reviews(reviews)
    end

    private

    def scraper
      @scraper ||= Scraper.new(
        scraper: Mechanize.new,
        base_url: 'https://www.dealerrater.com/dealer/McKaig-Chevrolet-Buick-A-Dealer-For-The-People-dealer-reviews-23685',
        pagination_param: '/page'
      )
    end

    def scrape_reviews(start_page: 1, end_page: 5)
      reviews = []

      puts "\nScrape Started!\n"
      start_page.upto(end_page) do |page_number|
        puts "Scraping reviews from page #{page_number}...\n"
        page = scraper.get_from_page(page_number)

        reviews << ReviewsBuilder.new(page).build
      end
      puts "Done!\n\n"

      reviews.flatten!
    end

    def filter_reviews(reviews)
      ReviewsFilter.by_severity(reviews)
    end

    def print_reviews(reviews)
      puts "Printing results:\n\n"
      reviews.each do |review|
        puts "Date: #{review.date}\n" +
          "Author: #{review.author}\n" +
          "Title: #{review.title}\n" +
          "Body: #{review.body}\n" +
          "Employee Reviews: #{review.employee_reviews.count}\n" +
          "Rating Average: #{review.rating}\n\n"
      end
    end
  end
end

Application.run
